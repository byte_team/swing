﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    [SerializeField] private Transform transformRoot;
    [SerializeField] private Slider slider;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private ParticleSystem particle2;
    [SerializeField] private bool autoRotate = false;
    [SerializeField] private int look = -1;
    [SerializeField] private float speed = 1;
    [SerializeField] private float speedTo =1;
    [SerializeField] private float range = 1;
    [SerializeField] private float offset = 0;
    private float targetAngle = 0;
    public void SetAutoRotate()
    {
        autoRotate = !autoRotate;
    }
    public void SetAngle(float val)
    {
        targetAngle =  Mathf.Clamp((val-offset)*range,-75,75);
        
    }
    public void SetRange(string val)
    {
        range = float.Parse(val);
    }
    public void SetOffset(string val)
    {
        offset = float.Parse(val);
    }

    public void SetScene()
    {
           if(SceneManager.GetActiveScene().buildIndex == 0)
            {
                SceneManager.LoadScene(1);
            }
            else { SceneManager.LoadScene(0); }

    }

    public void ChangeSpeed(float val)
    {
        speed = Mathf.Clamp(speed + val, 0, 1000);
    }

    public GameObject canvas;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            canvas.SetActive(!canvas.activeSelf);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SetScene();
        }
        if (Input.GetKey(KeyCode.Plus)|| Input.GetKey(KeyCode.KeypadPlus))
        {
            ChangeSpeed(1);
        }
        if (Input.GetKey(KeyCode.Minus)|| Input.GetKey(KeyCode.KeypadMinus))
        {
            ChangeSpeed(-1);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (autoRotate)
        {
            //   transformRoot.rotation = Quaternion.RotateTowards(transformRoot.rotation, Quaternion.Euler(transformRoot.eulerAngles.x+look, 0, 0), Time.deltaTime*speed);
            targetAngle += look * Time.deltaTime * speed;
        }
        if (slider.value >= slider.maxValue) look = -1;
        if (slider.value <= slider.minValue) look = 1;
        transformRoot.eulerAngles = new Vector3(targetAngle, 0, 0);
         //transformRoot.eulerAngles = Vector3.Lerp(new Vector3(transformRoot.eulerAngles.x,0,0), new Vector3(targetAngle,0,0),speedTo*Time.deltaTime);
       // particle.startColor = new Color(((slider.value+45)*2)/256,0, (255-(slider.value + 45) * 2)/256);
     //   particle2.startColor = new Color(((255 - (slider.value + 45) * 2) / 256), 0, (((slider.value + 45) * 2) / 256));
    }
}
