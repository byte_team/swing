﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Windows.Kinect;
using System.Linq;

public class DepthUse : MonoBehaviour
{
    public DepthViewMode ViewMode = DepthViewMode.MultiSourceReader;

    public GameObject DepthSourceManager;
    public GameObject MultiSourceManager;

    private KinectSensor _Sensor;
    private CoordinateMapper _Mapper;
    private Vector3[] _Vertices;
    private int[] _Triangles;

    // Only works at 4 right now
    [SerializeField] private int _DownsampleSize = 4;
    [SerializeField] private double _DepthScale = 0.1f;
    [SerializeField] private int _Speed = 50;
    [SerializeField] private Manager manager;
    private MultiSourceManager _MultiManager;
    private ColorSourceManager _ColorManager;
    private DepthSourceManager _DepthManager;
    public Vector2 partOfScreen = new Vector2(0.5f, 0.5f);

    public void SetSize(string val)
    {
        sizeSample = int.Parse(val);
    }

    void Start()
    {
        _Sensor = KinectSensor.GetDefault();
        if (_Sensor != null)
        {
            _Mapper = _Sensor.CoordinateMapper;
            var frameDesc = _Sensor.DepthFrameSource.FrameDescription;

            // Downsample to lower resolution
            CreateMesh(frameDesc.Width / _DownsampleSize, frameDesc.Height / _DownsampleSize);

            if (!_Sensor.IsOpen)
            {
                _Sensor.Open();
            }
        }
    }


    void CreateMesh(int width, int height)
    {
        
        _Vertices = new Vector3[width * height];
        _Triangles = new int[6 * ((width - 1) * (height - 1))];

        int triangleIndex = 0;
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int index = (y * width) + x;

                _Vertices[index] = new Vector3(x, -y, 0);
        
                // Skip the last row/col
                if (x != (width - 1) && y != (height - 1))
                {
                    int topLeft = index;
                    int topRight = topLeft + 1;
                    int bottomLeft = topLeft + width;
                    int bottomRight = bottomLeft + 1;

                    _Triangles[triangleIndex++] = topLeft;
                    _Triangles[triangleIndex++] = topRight;
                    _Triangles[triangleIndex++] = bottomLeft;
                    _Triangles[triangleIndex++] = bottomLeft;
                    _Triangles[triangleIndex++] = topRight;
                    _Triangles[triangleIndex++] = bottomRight;
                }
            }
        }
    }

    void Update()
    {
        if (_Sensor == null)
        {
            return;
        }


        if (ViewMode == DepthViewMode.SeparateSourceReaders)
        {

            if (DepthSourceManager == null)
            {
                return;
            }

            _DepthManager = DepthSourceManager.GetComponent<DepthSourceManager>();
            if (_DepthManager == null)
            {
                return;
            }

            gameObject.GetComponent<Renderer>().material.mainTexture = _ColorManager.GetColorTexture();
            RefreshData(_DepthManager.GetData(),
                _ColorManager.ColorWidth,
                _ColorManager.ColorHeight);
        }
        else
        {
            if (MultiSourceManager == null)
            {
                return;
            }

            _MultiManager = MultiSourceManager.GetComponent<MultiSourceManager>();
            if (_MultiManager == null)
            {
                return;
            }

//            gameObject.GetComponent<Renderer>().material.mainTexture = _MultiManager.GetColorTexture();

            RefreshData(_MultiManager.GetDepthData(),
                        _MultiManager.ColorWidth,
                        _MultiManager.ColorHeight);
        }
    }

    public float max = 1;
    public int sizeSample = 2;
    List<Vector3> VertecsSelect = new List<Vector3>();

    private void RefreshData(ushort[] depthData, int colorWidth, int colorHeight)
    {
        var frameDesc = _Sensor.DepthFrameSource.FrameDescription;
        
        ColorSpacePoint[] colorSpace = new ColorSpacePoint[depthData.Length];
     //   _Mapper.MapDepthFrameToColorSpace(depthData, colorSpace);
        VertecsSelect = new List<Vector3>();
        List<double> arrPoints = new List<double>();
        for (int y = 0; y < frameDesc.Height; y += _DownsampleSize)
        {
            for (int x = 0; x < frameDesc.Width; x += _DownsampleSize)
            {
                int indexX = x / _DownsampleSize;
                int indexY = y / _DownsampleSize;
                int smallIndex = (indexY * (frameDesc.Width / _DownsampleSize)) + indexX;

                double avg = GetAvg(depthData, x, y, frameDesc.Width, frameDesc.Height);
                avg = avg * _DepthScale;
                //if (max > (float)avg)
                //{
                    //Vertices[smallIndex].z = (float)avg;
                    VertecsSelect.Add(_Vertices[smallIndex]);
                if ((x >= frameDesc.Width * partOfScreen.x - _DownsampleSize *sizeSample && x <= frameDesc.Width * partOfScreen.x + _DownsampleSize * sizeSample) &&
                    (y >= frameDesc.Height * partOfScreen.y - _DownsampleSize * sizeSample && y <= frameDesc.Height * partOfScreen.y + _DownsampleSize * sizeSample))
                {
                    arrPoints.Add(avg);
                }// }

                // Update UV mapping with CDRP
                var colorSpacePoint = colorSpace[(y * frameDesc.Width) + x];
         //       _UV[smallIndex] = new Vector2(colorSpacePoint.X / colorWidth, colorSpacePoint.Y / colorHeight);
            }
        }
        manager.SetAngle((float)arrPoints.Sum()/arrPoints.Count);
        Debug.Log(arrPoints.Max());
        //Vector3 rez = Vector3.one;
        //foreach (var val in VertecsSelect)
        //{
        //    rez += val;
        //}

        //if (VertecsSelect.Count > 0)
        //{
        //    rez = rez / VertecsSelect.Count;
        //    Debug.Log(rez);
        //   
        //}

    }

    private double GetAvg(ushort[] depthData, int x, int y, int width, int height)
    {
        double sum = 0.0;

        for (int y1 = y; y1 < y + 4; y1++)
        {
            for (int x1 = x; x1 < x + 4; x1++)
            {
                int fullIndex = (y1 * width) + x1;

                if (depthData[fullIndex] == 0)
                    sum += 4500;
                else
                    sum += depthData[fullIndex];

            }
        }

        return sum / 16;
    }

    void OnApplicationQuit()
    {
        if (_Mapper != null)
        {
            _Mapper = null;
        }

        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }

            _Sensor = null;
        }
    }
}

