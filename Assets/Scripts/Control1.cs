﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control1 : MonoBehaviour
{
    Camera camera;
    [SerializeField] private Point point;
    // Start is called before the first frame update
    void Start()
    {
       camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
           
            point.SetPoint(hit.point);
        }
    }
}
