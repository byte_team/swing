﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    [SerializeField] private Transform begin;
    [SerializeField] private Transform body;
    [SerializeField] private Transform point;
    [SerializeField] private float correctSize =1;

    public void SetPoint(Vector3 _target)
    {
        point.position =  _target;
        var direction = (_target - transform.position).normalized;
        var _lookRotation = Quaternion.LookRotation(direction);
        point.rotation = _lookRotation;
        point.eulerAngles = _lookRotation.eulerAngles;// new Vector3(0, _lookRotation.eulerAngles.y, _lookRotation.eulerAngles.z);
        body.rotation = _lookRotation;
        var dif = _target - begin.position;
        
        body.position = begin.position+(dif*0.5f);
        body.localScale = new Vector3(1, 1, Vector3.Distance(begin.position, _target)*correctSize);
    }
}
